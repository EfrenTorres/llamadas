package com.example.examplebd;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText txtNombre, txtTelefono, txtCorreo;
    Button btnGuardar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtNombre=(EditText)findViewById(R.id.txtNombre);
        txtTelefono=(EditText)findViewById(R.id.txtTelefono);
        txtCorreo=(EditText)findViewById(R.id.txtCorreo);
        btnGuardar=(Button)findViewById(R.id.btnGuardar);

        final bdManager bd = new bdManager(getApplicationContext());

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bd.agregarContacto(txtNombre.getText().toString(),txtTelefono.getText().toString(),txtCorreo.getText().toString());
                Toast.makeText(getApplicationContext(),"SE AGREGO CORRECTAMENTE",Toast.LENGTH_SHORT).show();
            }
        });
    }
}